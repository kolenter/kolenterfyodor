package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    private Matrix matrix;

    public PyramidBuilder() {

    }

    /**
     * Build a pyramid from given input list of integer values.
     * Numbers are sorted ascending from top to bottom, from left to right.
     * @return 2D array with the pyramid.
     * @throws CannotBuildPyramidException if it's not possible to build a pyramid.
     */
    public int[][] buildPyramid(final List<Integer> input) throws CannotBuildPyramidException {
        if (input == null || input.isEmpty()) {
            throw new CannotBuildPyramidException();
        }

        if (input.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        this.matrix = new Matrix(input);
        if (!matrix.isPyramid()) {
            throw new CannotBuildPyramidException();
        }
        if (matrix.getRows() <=0 || matrix.getColumns() <=0 ){
            throw new CannotBuildPyramidException();
        }

        int[][] pyramid = generateEmptyPyramid(matrix.getRows(), matrix.getColumns());
        final List<Integer> sortedInput = sort(matrix.getInput());
        final int center = matrix.getColumns() / 2;

        for (int i = 0, offset = 0, count = 1, arrIdx = 0; i < matrix.getRows(); i++, offset++, count++) {
            int start = center - offset;
            for (int j = 0; j < count * 2; j += 2, arrIdx++) {
                pyramid[i][start + j] = sortedInput.get(arrIdx);
            }
        }
        return pyramid;
    }

    private List<Integer> sort(List<Integer> list) {
        List<Integer> sortedList = new ArrayList<Integer>(list);
        Collections.sort(sortedList);
        return sortedList;
    }

    private int[][] generateEmptyPyramid(int rows, int columns) {
        int[][] pyramid = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                pyramid[i][j] = 0;
            }
        }
        return pyramid;
    }
}
