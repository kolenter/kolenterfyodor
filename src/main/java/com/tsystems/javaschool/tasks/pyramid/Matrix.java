package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

class Matrix {

    private final List<Integer> input;
    private final int rows;
    private final int columns;
    private final boolean pyramid;

    Matrix(List<Integer> input) {
        int count = 0;
        int rows = 1;
        int columns = 1;
        while (count < input.size()) {
            count = count + rows;
            rows++;
            columns = columns + 2;
        }

        this.input = input;
        this.rows = rows - 1;
        this.columns = columns - 2;
        this.pyramid = count == input.size();
    }

    /**
     * Returns input list of integer values.
     * @return see description.
     */
    List<Integer> getInput() {
        return input;
    }

    /**
     * Returns amount of the rows.
     * @return see description.
     */
    int getRows() {
        return rows;
    }

    /**
     * Returns amount of the columns.
     * @return see description.
     */
    int getColumns() {
        return columns;
    }

    /**
     * Returns whether current matrix is pyramid or not.
     * @return see description.
     */
    boolean isPyramid() {
        return pyramid;
    }
}
