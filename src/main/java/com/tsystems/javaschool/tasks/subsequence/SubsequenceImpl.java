package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class SubsequenceImpl implements Subsequence
{
    public SubsequenceImpl()
    {}
    @Override
    public boolean find(List x, List y) {
        if (x == null) {
            throw new IllegalArgumentException("x cannot be null");
        }
        if (y == null) {
            throw new IllegalArgumentException("y cannot be null");
        }

        int lastIndex = 0;
        for (Object elementX : x) {
            List subListY = y.subList(lastIndex, y.size());
            if (subListY.contains(elementX)) {
                lastIndex += subListY.indexOf(elementX);

            } else {
                return false;
            }
        }
        return true;

    }

}
//