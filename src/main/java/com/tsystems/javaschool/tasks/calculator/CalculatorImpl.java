
package com.tsystems.javaschool.tasks.calculator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;
import java.util.Locale;
import java.util.NoSuchElementException;


public class CalculatorImpl implements Calculator {


    public static final String OUT_NUMBER_PATTERN = "#.####";

    public static Symbols isOperator(char c) { //Если символ - оператор
        if (c == Symbols.PLUS.getSymbol()) {
            return Symbols.PLUS;
        } else if (c == Symbols.MINUS.getSymbol()) {
            return Symbols.MINUS;
        } else if (c == Symbols.MULTIPLY.getSymbol()) {
            return Symbols.MULTIPLY;
        } else if (c == Symbols.DIVIDE.getSymbol()) {
            return Symbols.DIVIDE;
        }
        return null;
    }

    public static int priority(Symbols op) { //Определение приоритетов операций
        switch (op) {
            case PLUS:
            case MINUS:
                return 1;
            case MULTIPLY:
            case DIVIDE:
                return 2;
        }
        return -1;
    }

    public static void main(String[] args) throws Exception {
        CalculatorImpl calculator = new CalculatorImpl();
        String s1 = "7 + (5*2*8) + 1/5156165";
        System.out.println(s1 + " = " + calculator.evaluate(s1));
        String s2 = ".10/2.5+15+0.001";
        System.out.println(s2 + " = " + calculator.evaluate(s2));
        String s3 = "(1+38)*4-5";
        System.out.println(s3 + " = " + calculator.evaluate(s3));
        String s4 = "7*6/2+8";
        System.out.println(s4 + " = " + calculator.evaluate(s4));
        String s5 = "(+2.33333333) - 2";
        System.out.println(s5 + " = " + calculator.evaluate(s5));
    }

    public static double summation(double a, double b) {
        return a + b;
    }

    public static double subtraction(double a, double b) {
        return a - b;
    }

    public static double multiplication(double a, double b) {
        return a * b;
    }

    public static Double division(double a, double b) {
        if (b == 0) {
            return null;
        }
        return a / b;
    }

    public static boolean processOperator(LinkedList<Double> st, Symbols op) { //Выполнение арифметических операций
        double r = st.removeLast(); //первый операнд
        double l = st.removeLast(); //второй операнд
        Double res = null;
        try {
            res = (Double) op.getMethod().invoke(null, l, r);
        } catch (IllegalArgumentException ignored) {
        } catch (IllegalAccessException ignored) {
        } catch (InvocationTargetException ignored) {
        }
        st.add(res);
        return res != null;
    }

    @Override
    public String evaluate(String s) {
        if (s == null || s.isEmpty()){
            return null;
        }
        s = s.replaceAll(" ", "");
        Boolean wasSign = true;
        LinkedList<Double> st = new LinkedList<Double>(); //операнды
        LinkedList<Symbols> op = new LinkedList<Symbols>(); //операции
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == Symbols.OPEN_BRACKET.getSymbol()) {
                wasSign = true;
                op.add(Symbols.OPEN_BRACKET);
                continue;
            } else if (c == Symbols.CLOSE_BRACKET.getSymbol()) { //если скобка закрылась, ищем среди операций открытую скобку
                try {
                    while (op.getLast() != Symbols.OPEN_BRACKET) {
                        if (!processOperator(st, op.removeLast())) { //выполняем операции в скобке
                            return null;
                        }
                    }
                    op.removeLast(); //выкидываем скобку из списка операций
                } catch (NoSuchElementException e) {    //Ошибка закрытой скобки
                    return null;
                }
            } else {
                Symbols symbol = isOperator(c);
                if (symbol != null && !((symbol == Symbols.MINUS || symbol == Symbols.PLUS) && wasSign)) {
                    //если символ - оператор
                    try {
                        while (!op.isEmpty() && priority(op.getLast()) >= priority(symbol)) { //работаем с приоритетами
                            if (!processOperator(st, op.removeLast())) {
                                return null;
                            }
                        }
                        op.add(symbol);//добавляем новую операцию
                    } catch (NoSuchElementException e) { //   если дублируются знаки
                        return null;
                    }
                } else {  //если символ - цифра
                    String operand = "";
                    if (s.charAt(i) == Symbols.MINUS.getSymbol() || s.charAt(i) == Symbols.PLUS.getSymbol()) {
                        operand += s.charAt(i++);
                    }
                    while (i < s.length() && (Character.isDigit(s.charAt(i)) || s.charAt(i) == '.')) {
                        operand += s.charAt(i++);
                    }
                    --i;
                    try {
                        st.add(Double.parseDouble(operand));//добавляем операнд
                    } catch (NumberFormatException e) {  //если неизвестный символ

                        return null;
                    }
                }
            }
            wasSign = false;
        }
        try {
            while (!op.isEmpty()) { //выполняем операции до последней
                if (!processOperator(st, op.removeLast())) {
                    return null;
                }
            }
        } catch (NoSuchElementException e) { //Ошибка открытой скобки
            return null;
        } catch (ArithmeticException e) { //Ошибка деления на ноль
            return null;
        }

        return roundNumber(st.get(0));
    }

    public static String roundNumber(double number) {
        DecimalFormat decimalFormat = new DecimalFormat(OUT_NUMBER_PATTERN, DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        return decimalFormat.format(number);
    }

    public enum Symbols {
        PLUS('+', "summation"),
        MINUS('-', "subtraction"),
        MULTIPLY('*', "multiplication"),
        DIVIDE('/', "division"),
        OPEN_BRACKET('(', ""),
        CLOSE_BRACKET(')', "");

        private char symbol;
        private Method method;

        Symbols(char symbol, String name) {
            this.symbol = symbol;
            try {
                method = CalculatorImpl.class.getDeclaredMethod(name, double.class, double.class);
            } catch (SecurityException e) {
                method = null;
            } catch (NoSuchMethodException e) {
                method = null;
            }
        }

        public Method getMethod() {
            return method;
        }

        public char getSymbol() {
            return symbol;
        }
    }
}
